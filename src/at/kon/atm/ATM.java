package at.kon.atm;

public class ATM {
	private float balance;
	
	public void deposit(float amount) {
		balance += amount;
	}
	
	public void withdraw(float amount) {
		balance -= amount;
	}
	
	public float getBalance() {
		return balance;
	}
	
	public ATM() {
		balance = 0;
	}
	
}
