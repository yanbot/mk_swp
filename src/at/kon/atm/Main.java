package at.kon.atm;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		String input;
		
		ATM atm = new ATM();
		boolean running = true;
		
		
		while(running)
		{
			System.out.println("  1: Einzahlen");
			System.out.println("  2: Abheben");
			System.out.println("  3: Kontostand");
			System.out.println("  4: Beenden");
			input = scanner.nextLine();
			
			if(input.equals("1"))
			{
				System.out.println("Betrag den Sie einzahlen wollen:");
				input = scanner.nextLine();
				atm.deposit(Float.parseFloat(input));
				System.out.println("Sie haben "+input+"� eingezahlt.");
			} else if(input.equals("2"))
			{
				System.out.println("Betrag den Sie abheben wollen:");
				input = scanner.nextLine();
				atm.withdraw(Float.parseFloat(input));
				System.out.println("Sie haben "+input+"� abgehoben.");
			} else if(input.equals("3"))
			{
				System.out.println("Ihr Kontostand betr�gt: " + atm.getBalance()+"�");
			} else if(input.equals("4"))
			{
				System.out.println("System wird beendet");
				running = false;
			}
		}
		
		
	}

}
