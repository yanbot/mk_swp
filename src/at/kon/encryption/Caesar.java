package at.kon.encryption;

public class Caesar implements Encryptor {

	public String encrypt(String input) {
		char[] encrypted = input.toCharArray();
		
		for(int i = 0; i < encrypted.length; i++)
		{
			// Im Bereich der Großbuchstaben
			if(encrypted[i] >= 65 && encrypted[i] <= 90) {
				// Um 3 Verschieben
				encrypted[i] = (char) (encrypted[i] + 3);
				
				// Bei Überlaufen
				if(encrypted[i] > 90) encrypted[i] = (char) (encrypted[i] - 26); 
			} 
			
			// Im Bereich der Kleinbuchstaben
			else if(encrypted[i] >= 97 && encrypted[i] <= 122) {
				// Um 3 Verschieben
				encrypted[i] = (char) (encrypted[i] + 3);
				
				// Bei Überlaufen
				if(encrypted[i] > 122) encrypted[i] = (char) (encrypted[i] - 26); 
			}
		}
		return new String(encrypted);
	}

	public String decrypt(String input) {
		char[] decrypted = input.toCharArray();
		
		for(int i = 0; i < decrypted.length; i++)
		{	
			// Im Bereich der Großbuchstaben
			if(decrypted[i] >= 65 && decrypted[i] <= 90) {
				// Um 3 Verschieben
				decrypted[i] = (char) (decrypted[i] - 3);
				
				// Bei Unterlaufen
				if(decrypted[i] < 65) decrypted[i] += 26; 
			} 
			
			// Im Bereich der Kleinbuchstaben
			else if(decrypted[i] >= 97 && decrypted[i] <= 122) {
				// Um 3 Verschieben
				decrypted[i] = (char) (decrypted[i] - 3);
				
				// Bei Unterlaufen
				if(decrypted[i] < 97) decrypted[i] += 26; 
			}
		}
		return new String(decrypted);
	}
}
