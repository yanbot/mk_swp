package at.kon.encryption;

public interface Encryptor {
	public String encrypt(String input);
	public String decrypt(String input); 
}
