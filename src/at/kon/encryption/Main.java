package at.kon.encryption;



public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Encryptor encryptor = new Caesar();
		
		String string = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz";
		String encrypted = encryptor.encrypt(string);
		String decrypted = encryptor.decrypt(encrypted);
		
		System.out.println("Input: " + string);
		System.out.println("Encryption Output: " + encrypted);
		System.out.println("Decryption Output: " + decrypted);
	}

}
